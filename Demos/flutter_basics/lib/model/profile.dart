class Profile {
  String name;
  String email;
  String phoneNumber;

  Profile({required this.name, required this.email, required this.phoneNumber});
}
