import 'package:flutter/material.dart';

import '../model/profile.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  late Profile userProfile;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  bool _isEditing = false;

  @override
  void initState() {
    super.initState();
    // Initialize user profile data
    userProfile = Profile(
        name: 'John Doe',
        email: 'john.doe@example.com',
        phoneNumber: '1234567890');

    // Set initial values to text controllers
    _nameController.text = userProfile.name;
    _emailController.text = userProfile.email;
    _phoneController.text = userProfile.phoneNumber;
  }

  void _toggleEditing() {
    setState(() {
      _isEditing = !_isEditing;
    });
  }

  void _saveProfile() {
    setState(() {
      userProfile.name = _nameController.text;
      userProfile.email = _emailController.text;
      userProfile.phoneNumber = _phoneController.text;
      _isEditing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile Page'),
        actions: [
          IconButton(
            icon: Icon(_isEditing ? Icons.save : Icons.edit),
            onPressed: _toggleEditing,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: CircleAvatar(
                radius: 50,
                backgroundImage:
                    AssetImage('assets/ww.png'), // Add your image asset path
              ),
            ),
            SizedBox(height: 20),
            TextField(
              controller: _nameController,
              decoration:
                  InputDecoration(labelText: 'Name', enabled: _isEditing),
              enabled: _isEditing,
            ),
            SizedBox(height: 20),
            TextField(
              controller: _emailController,
              decoration:
                  InputDecoration(labelText: 'Email', enabled: _isEditing),
              enabled: _isEditing,
            ),
            SizedBox(height: 20),
            TextField(
              controller: _phoneController,
              decoration: InputDecoration(
                  labelText: 'Phone Number', enabled: _isEditing),
              enabled: _isEditing,
            ),
            SizedBox(height: 20),
            Center(
              child: ElevatedButton(
                onPressed: !_isEditing ? null : _saveProfile,
                child: Text('Save'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
