import 'package:flutter/material.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({super.key});

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  TextEditingController _userInputTextController = TextEditingController();
  String _message = "";
  String _magicNumber = "7";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void _checkValue() {
    if (_userInputTextController.text == _magicNumber) {
      setState(() {
        _message = "Congratulations";
      });
    } else {
      setState(() {
        _message = "Try again";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Let us play a game!'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _userInputTextController,
              decoration: InputDecoration(label: Text('Enter your guess')),
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(onPressed: _checkValue, child: Text('check')),
            SizedBox(
              height: 20,
            ),
            Text('$_message')
          ],
        ),
      ),
    );
  }
}
