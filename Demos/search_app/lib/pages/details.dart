import 'package:flutter/material.dart';
import 'package:search_app/data/list.dart';

class RestDetailsPage extends StatefulWidget {
  Resturant? restDetails = null;
  RestDetailsPage({super.key, this.restDetails});

  @override
  State<RestDetailsPage> createState() => _RestDetailsPageState();
}

class _RestDetailsPageState extends State<RestDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.restDetails!.name),
      ),
      body: Center(
        child: Column(
          children: [
            Text(widget.restDetails!.name),
            Text(widget.restDetails!.phone),
            Text(widget.restDetails!.address),
            ElevatedButton(
                onPressed: () {
                  resturants.remove(widget.restDetails);
                  Navigator.pop(context);
                },
                child: Text('Remove'))
          ],
        ),
      ),
    );
  }
}
