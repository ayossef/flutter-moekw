import 'package:flutter/material.dart';
import 'package:search_app/widgets/add.dart';
import 'package:search_app/widgets/search.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> screens = [const SearchWidget(), const AddWidget()];
  int _currentScreenIndex = 0;
  void _updateSelectedScreen(int newIndex) {
    setState(() {
      _currentScreenIndex = newIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentScreenIndex,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Add')
        ],
        onTap: (value) {
          _updateSelectedScreen(value);
        },
      ),
      appBar: AppBar(
        title: const Text('Al-Mubrakeya'),
      ),
      body: Center(
        child: Column(
          children: [
            screens[_currentScreenIndex],
          ],
        ),
      ),
    );
  }
}
