import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:search_app/data/list.dart';
import 'package:search_app/data/list_manager.dart';

class AddWidget extends StatefulWidget {
  const AddWidget({super.key});

  @override
  State<AddWidget> createState() => _AddWidgetState();
}

class _AddWidgetState extends State<AddWidget> {
  TextEditingController _restNameCont = TextEditingController();
  TextEditingController _restPhoneCont = TextEditingController();
  TextEditingController _restAddressController = TextEditingController();

  String _msg = "";

  void _addAction() {
    String restName = _restNameCont.text;
    String restPhone = _restPhoneCont.text;
    String restAddress = _restAddressController.text;

    bool valueExists = ListManager().isRestFound(restName);
    if (valueExists) {
      _setMessage("Item Already Exists");
    } else {
      Resturant newRest =
          Resturant(address: restAddress, name: restName, phone: restPhone);
      resturants.add(newRest);
      _clearAll();
      _setMessage("Item has been succesfully added");
    }
    print(resturants);
  }

  void _setMessage(String value) {
    setState(() {
      _msg = value;
    });
  }

  void _clearAll() {
    _restAddressController.text = "";
    _restNameCont.text = "";
    _restPhoneCont.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          TextField(
            controller: _restNameCont,
            decoration: InputDecoration(hintText: 'Name'),
          ),
          TextField(
            controller: _restPhoneCont,
            decoration: InputDecoration(hintText: 'Phone'),
          ),
          TextField(
            decoration: InputDecoration(hintText: 'Address'),
            controller: _restAddressController,
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(onPressed: _addAction, child: Text('Add')),
          SizedBox(
            height: 20,
          ),
          Text(_msg)
        ],
      ),
    );
  }
}
