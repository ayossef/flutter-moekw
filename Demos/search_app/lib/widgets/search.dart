import 'package:flutter/material.dart';
import 'package:search_app/data/list.dart';
import 'package:search_app/data/list_manager.dart';
import 'package:search_app/pages/details.dart';

class SearchWidget extends StatefulWidget {
  const SearchWidget({super.key});

  @override
  State<SearchWidget> createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  TextEditingController _searchInputCont = TextEditingController();
  String _msg = "";
  bool _found = false;

  void _searchAction() {
    String searchValue = _searchInputCont.text;
    bool found = ListManager().isRestFound(searchValue);
    if (found) {
      _setFoundFlag();
      _setMessage("Item is found");
    } else {
      _clearFoundFlag();
      _setMessage("Item is not found");
    }
  }

  void _setFoundFlag() {
    setState(() {
      _found = true;
    });
  }

  void _clearFoundFlag() {
    setState(() {
      _found = false;
    });
  }

  void _setMessage(String msgValue) {
    setState(() {
      _msg = msgValue;
    });
  }

  void _showDetails() {
    String userSearchValue = _searchInputCont.text;
    Resturant restTobeRemoved = ListManager().getRestByName(userSearchValue)!;
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => RestDetailsPage(
                restDetails: restTobeRemoved,
              )),
    );

    // resturants.remove(restTobeRemoved);
    // _searchAction();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          TextField(
            controller: _searchInputCont,
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
            onPressed: _searchAction,
            child: Text('Search'),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(_msg),
              SizedBox(
                width: 20,
              ),
              _found
                  ? ElevatedButton(
                      onPressed: _showDetails, child: Text('Show Details'))
                  : SizedBox()
            ],
          ),
        ],
      ),
    );
  }
}
