import 'package:flutter/material.dart';

class GamePage extends StatefulWidget {
  const GamePage({super.key});

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  String _magicNumber = "13";
  String _msg = "";
  TextEditingController _userInputController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _msg = "Enter your guess";
  }

  void _guessAction() {
    print(_userInputController.text);
    if (_userInputController.text == _magicNumber) {
      setState(() {
        _msg = "Congratulations !! ✨";
      });
    } else {
      setState(() {
        _msg = "Try again 😿";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Game'),
      ),
      body: Center(
        child: Column(
          children: [
            TextField(
              controller: _userInputController,
            ),
            SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: _guessAction,
              child: Text('Check My Guess'),
            ),
            SizedBox(
              height: 20,
            ),
            Text('$_msg')
          ],
        ),
      ),
    );
  }
}
