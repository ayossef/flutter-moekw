import 'dart:math';

import 'package:flutter/material.dart';

class AdvGame extends StatefulWidget {
  const AdvGame({super.key});

  @override
  State<AdvGame> createState() => _AdvGameState();
}

class _AdvGameState extends State<AdvGame> {
  String _msg = "";
  String _magicNumber = "";
  TextEditingController _cont = TextEditingController();
  int _numberOfRemainingTrails = 5;

  int getRandomNumber() {
    return Random().nextInt(10);
  }

  void _guessAction() {
    _numberOfRemainingTrails -= 1;
    if (_cont.text == _magicNumber) {
      setState(() {
        _msg = "Congratulations 🎉";
      });
    } else {
      setState(() {
        _msg = "Try again ☠️";
      });
    }
  }

  void _resetAction() {
    setState(() {
      _numberOfRemainingTrails = 5;
      _msg = "";
      _cont.text = "";
      _magicNumber = "$getRandomNumber()";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Adv Game'),
      ),
      body: Center(
        child: Column(
          children: [
            TextField(
              controller: _cont,
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: _numberOfRemainingTrails == 0 ? null : _guessAction,
                child: Text('Guess')),
            SizedBox(
              height: 20,
            ),
            Text('$_msg'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _resetAction,
        child: Icon(Icons.refresh),
      ),
    );
  }
}
