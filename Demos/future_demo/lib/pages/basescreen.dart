import 'package:flutter/material.dart';

class BaseScreen extends StatefulWidget {
  String title;
  Widget mainContent;

  BaseScreen({super.key, required this.title, required this.mainContent});

  @override
  State<BaseScreen> createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amberAccent,
        title: Text(widget.title),
      ),
      body: Center(
        child: widget.mainContent,
      ),
    );
  }
}
