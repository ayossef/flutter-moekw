import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:future_demo/pages/basescreen.dart';
import 'package:future_demo/widgets/details.dart';
import 'package:future_demo/widgets/header.dart';

import '../model/login.dart';

class FutureHome extends StatefulWidget {
  const FutureHome({super.key});

  @override
  State<FutureHome> createState() => _FutureHomeState();
}

class _FutureHomeState extends State<FutureHome> {
  Future<String>? futureString;
  Future<String> loadData() async {
    await Future.delayed(Duration(seconds: 5));
    return "Data has been recieved";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureString = loadData();
  }

  Future<bool>? _loginResult;
  void _loginAction() {
    setState(() {
      _loginResult = login("admin", "admin");
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        title: 'Future Home',
        mainContent: Column(children: [
          ElevatedButton(
              onPressed: () {
                loadData();
              },
              child: Text('Load Data')),
          FutureBuilder(
              future: login("admin", "admin"),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text('Error ${snapshot.error}');
                }
                if (snapshot.hasData) {
                  if (snapshot.data!) {
                    return Text('Valid Login');
                  } else {
                    return Text('Invlaid Login');
                  }
                } else {
                  return Text('Loading ..');
                }
              })
        ]));
  }
}
