import 'package:flutter/material.dart';
import 'package:future_demo/model/login.dart';
import 'package:future_demo/pages/basescreen.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameCont = TextEditingController();
  final TextEditingController _passwordCont = TextEditingController();
  Future<bool>? _loginResult;
  bool _loginView = true;

  void _loginAction() {
    setState(() {
      _loginResult = login(_usernameCont.text, _passwordCont.text);
    });
  }

  void _logoutAction() {
    setState(() {
      _loginResult = null;
      _loginView = true;
      _passwordCont.text = "";
      _usernameCont.text = "";
    });
  }

  void _updateScreen() async {
    await Future.delayed(const Duration(milliseconds: 1));
    setState(() {
      // _loginView = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        title: 'Login',
        mainContent: _loginView
            ? Column(
                children: [
                  TextField(
                    controller: _usernameCont,
                  ),
                  TextField(
                    controller: _passwordCont,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: ElevatedButton(
                        onPressed: _loginAction, child: const Text('Login')),
                  ),
                  FutureBuilder(
                    future: _loginResult,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState.index == 1) {
                        return Container(
                            padding: EdgeInsets.all(4),
                            child: const Text(
                              'Loading ..',
                            ));
                      }
                      if (snapshot.hasError) {
                        return Text('Error ${snapshot.error}');
                      }
                      if (snapshot.hasData) {
                        if (snapshot.data!) {
                          _loginView = false;
                          _updateScreen();
                          return const Text('Valid Login');
                        } else {
                          return const Text('Invalid Login');
                        }
                      }
                      return const Text("");
                    },
                    initialData: null,
                  )
                ],
              )
            : Column(
                children: [
                  Text('Welcome to your home ${_usernameCont.text}'),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: ElevatedButton(
                        onPressed: _logoutAction, child: const Text('Logout')),
                  )
                ],
              ));
  }
}
