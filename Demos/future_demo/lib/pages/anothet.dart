import 'package:flutter/material.dart';
import 'package:future_demo/pages/basescreen.dart';

import '../widgets/details.dart';
import '../widgets/header.dart';

class AnotherScreen extends StatefulWidget {
  const AnotherScreen({super.key});

  @override
  State<AnotherScreen> createState() => _AnotherScreenState();
}

class _AnotherScreenState extends State<AnotherScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        title: 'Another Screen',
        mainContent: Column(
          children: [
            HeaderText(value: "Main Header"),
            DetailText(
                value: "Some detailed content in the body of the screen"),
            DetailText(
              value: "Some detailed content in the body of the screen",
              fontColor: Colors.red,
            ),
            DetailText(
                value: "Some detailed content in the body of the screen"),
            DetailText(
                value: "Some detailed content in the body of the screen"),
            DetailText(
                value: "Some detailed content in the body of the screen"),
          ],
        ));
  }
}
