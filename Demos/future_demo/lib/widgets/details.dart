import 'package:flutter/material.dart';

class DetailText extends StatelessWidget {
  String value;
  Color? fontColor;
  DetailText({super.key, required this.value, this.fontColor = Colors.red});

  @override
  Widget build(BuildContext context) {
    return Text(
      value,
      style: TextStyle(fontSize: 18, color: fontColor),
    );
  }
}
