import 'package:flutter/material.dart';

import '../model/login.dart';

class LoginWidget extends StatefulWidget {
  const LoginWidget({super.key});

  @override
  State<LoginWidget> createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  TextEditingController _usernameCont = TextEditingController();
  TextEditingController _passwordCont = TextEditingController();
  Future<bool>? _loginResult;

  void _loginAction() {
    setState(() {
      _loginResult = login(_usernameCont.text, _passwordCont.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: _usernameCont,
        ),
        TextField(
          controller: _passwordCont,
        ),
        ElevatedButton(onPressed: _loginAction, child: const Text('Login')),
        FutureBuilder(
            future: _loginResult,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Text('Loading ...');
              }
              if (snapshot.hasError) {
                return Text('Error ${snapshot.error}');
              }
              if (snapshot.data!) {
                return const Text('Valid Login');
              } else {
                return const Text('Invalid Login');
              }
            })
      ],
    );
  }
}
