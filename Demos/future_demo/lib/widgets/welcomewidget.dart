import 'package:flutter/material.dart';

class WelcomeWidget extends StatefulWidget {
  const WelcomeWidget({super.key});

  @override
  State<WelcomeWidget> createState() => _WelcomeWidgetState();
}

class _WelcomeWidgetState extends State<WelcomeWidget> {
  void _logoutAction() {}
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('Welcome to your home page'),
        ElevatedButton(onPressed: _logoutAction, child: Text('Logout'))
      ],
    );
  }
}
