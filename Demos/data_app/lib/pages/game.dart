import 'package:flutter/material.dart';

class GamePage extends StatefulWidget {
  const GamePage({super.key});

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  bool _newGame = true;
  



  void _newGameAction() {
    setState(() {
      _newGame = false;
    });
  }

  

  void _clearAll() {
    setState(() {
      _msg = "";
      _guessContr.text = "";
      _numberOfRemainingTrails = 5;
    });
  }

  

  void _resetGameAction() {
    setState(() {
      _newGame = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Let us Play a Game!'),
      ),
      body: Center(
          child: _newGame
              ? Column(
                  children: [
                    TextField(
                      controller: _usernameCont,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                        onPressed: _newGameAction, child: Text('New Game'))
                  ],
                )
              : ),
    );
  }
}
