import 'package:flutter/material.dart';

class PlayGame extends StatefulWidget {
  const PlayGame({super.key});

  @override
  State<PlayGame> createState() => _PlayGameState();
}

class _PlayGameState extends State<PlayGame> {
  TextEditingController _guessContr = TextEditingController();
  TextEditingController _usernameCont = TextEditingController();
  String _msg = "";
  int _magicNumber = 7;
  int _numberOfRemainingTrails = 5;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _magicNumber = 7;
    _numberOfRemainingTrails = 5;
    _msg = "";
  }

  int _getScore() {
    return 10 * _numberOfRemainingTrails;
  }

  String _getUsername() {
    return _usernameCont.text;
  }

  String _getCongratsMessage() {
    String username = _getUsername();
    int score = _getScore();
    return "Congtulations, $username, You have scored, $score";
  }

  String _getTryAgainMessage() {
    return "Try again, you have $_numberOfRemainingTrails remaning trails";
  }

  void _guessAction() {
    // check the number
    _numberOfRemainingTrails -= 1;
    if (int.parse(_guessContr.text) == _magicNumber) {
      setState(() {
        _msg = _getCongratsMessage();
      });
    } else {
      setState(() {
        _msg = _getTryAgainMessage();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: _guessContr,
        ),
        SizedBox(
          height: 20,
        ),
        ElevatedButton(onPressed: _guessAction, child: Text('Guess')),
        SizedBox(
          height: 20,
        ),
        Text(_msg),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
