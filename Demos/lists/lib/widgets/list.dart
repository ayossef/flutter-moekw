import 'package:flutter/material.dart';
import 'package:lists/model/data.dart';

class ListWidget extends StatefulWidget {
  const ListWidget({super.key});

  @override
  State<ListWidget> createState() => _ListWidgetState();
}

class _ListWidgetState extends State<ListWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            malls[index],
            style: const TextStyle(fontSize: 18, color: Colors.blue),
          ),
          leading: const Icon(Icons.star),
        );
      },
      itemCount: malls.length,
    );
  }
}
