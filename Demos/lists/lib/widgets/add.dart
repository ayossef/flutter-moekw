import 'package:flutter/material.dart';

class AddWidget extends StatefulWidget {
  const AddWidget({super.key});

  @override
  State<AddWidget> createState() => _AddWidgetState();
}

class _AddWidgetState extends State<AddWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            'Add Page',
            style: TextStyle(color: Colors.blue, fontSize: 30),
          )
        ],
      ),
    );
  }
}
