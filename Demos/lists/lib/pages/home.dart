import 'package:flutter/material.dart';
import 'package:lists/widgets/add.dart';
import 'package:lists/widgets/list.dart';
import 'package:lists/widgets/search.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentSelectedIndex = 0;
  List<Widget> screens = [
    const SearchWidget(),
    const AddWidget(),
    const ListWidget()
  ];
  List<String> screenTitles = ['Search', 'Add', 'List'];
  void _updateSelectedIndex(int newIndexValue) {
    setState(() {
      _currentSelectedIndex = newIndexValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(screenTitles[_currentSelectedIndex]),
        backgroundColor: Colors.blue,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
          BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Add'),
          BottomNavigationBarItem(icon: Icon(Icons.list), label: 'List'),
        ],
        currentIndex: _currentSelectedIndex,
        onTap: (value) {
          _updateSelectedIndex(value);
        },
      ),
      body: screens[_currentSelectedIndex],
    );
  }
}
