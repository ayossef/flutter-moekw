import 'dart:convert';

import 'package:http/http.dart' as http;

Future<Map> loadUserName(String url) async {
  String? userName;
  // 1. Get the response
  http.Response resp = await http.get(Uri.parse(url));

  // 2. check status code
  if (resp.statusCode == 200) {
    // 3. read resp body
    String respBody = resp.body;

    // 4. convert resp body to map or list
    Map<String, dynamic> mapData = jsonDecode(respBody);

    // 5. convert map or list to an object
    return mapData;
    // 6. return
  }
  return Map();
}
