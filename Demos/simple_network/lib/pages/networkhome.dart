import 'package:flutter/material.dart';
import 'package:simple_network/model/networklaoder.dart';

class NetworkHome extends StatefulWidget {
  const NetworkHome({super.key});

  @override
  State<NetworkHome> createState() => _NetworkHomeState();
}

class _NetworkHomeState extends State<NetworkHome> {
  Future<Map>? _userData;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _userData = loadUserName("https://jsonplaceholder.typicode.com/users/1");
  }

  void _reload() {
    setState(() {
      _userData = loadUserName("https://jsonplaceholder.typicode.com/users/1");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simple Network Demo'),
      ),
      body: Center(
        child: Column(
          children: [
            Text("Networking and APIs in Flutter is easy and Fun"),
            FutureBuilder(
                initialData: null,
                future: _userData,
                builder: ((context, snapshot) {
                  if (snapshot.connectionState.index == 1) {
                    return Text("Loading");
                  }
                  if (snapshot.hasData) {
                    return Text(snapshot.data!["name"]);
                  }
                  return Text("");
                }))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _reload,
        child: Icon(Icons.refresh),
      ),
    );
  }
}
