import 'dart:convert';

import 'student.dart';
import 'package:http/http.dart' as http;

class StudentNetwrokManager {
  String baseUrl;
  StudentNetwrokManager({required this.baseUrl});
  Future<Student> createStudent(Student toBeCreated) async {
    Map<String, dynamic> mapData = toBeCreated.toJson();
    var headers = {'Content-type': 'applcation/json'};
    String bodyString = jsonEncode(mapData);
    http.Response resp =
        await http.post(Uri.parse(baseUrl), body: bodyString, headers: headers);
    if (resp.statusCode == 201) {
      String respBody = resp.body;
      Map<String, dynamic> newStudentData = jsonDecode(respBody);
      return Student.fromJson(newStudentData);
    }
    throw Error();
  }
}
