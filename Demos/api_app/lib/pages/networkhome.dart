import 'package:api_app/model/dataloader.dart';
import 'package:flutter/material.dart';

import '../model/user.dart';

class NetworkHome extends StatefulWidget {
  const NetworkHome({super.key});

  @override
  State<NetworkHome> createState() => _NetworkHomeState();
}

class _NetworkHomeState extends State<NetworkHome> {
  Future<User>? _userDataFuture;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _userDataFuture = loadData("https://jsonplaceholder.typicode.com/users/1");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Network Demo'),
      ),
      body: Center(
        child: Column(
          children: [
            FutureBuilder(
                future: _userDataFuture,
                builder: ((context, snapshot) {
                  if (snapshot.connectionState.index == 0) {
                    return const Text("");
                  }
                  if (snapshot.connectionState.index == 1) {
                    return const Text("Loading..");
                  }
                  if (snapshot.hasData) {
                    return Column(
                      children: [Text(snapshot.data!.name)],
                    );
                  }
                  return const Text("");
                }))
          ],
        ),
      ),
    );
  }
}
