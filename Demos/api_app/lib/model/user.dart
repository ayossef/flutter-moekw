class User {
  int id;
  String name;
  String email;
  String phoneNumber;

  User(
      {required this.id,
      required this.name,
      required this.email,
      required this.phoneNumber});

  factory User.fromJSON(Map<String, dynamic> data) {
    return User(
        id: data["id"],
        name: data["name"],
        email: data["email"],
        phoneNumber: data["phone"]);
  }
}
