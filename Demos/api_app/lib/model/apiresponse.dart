class APIResponse {
  List<String> it;
  List<String> ops;

  APIResponse({required this.it, required this.ops});

  factory APIResponse.fromJSON(Map<String, List<String>> map) {
    return APIResponse(
        it: map["إدارة نظم المعلومات"]!, ops: map["إدارة العمليات"]!);
  }
}
