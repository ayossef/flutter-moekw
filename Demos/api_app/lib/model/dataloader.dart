import 'dart:convert';
import 'package:http/http.dart' as http;
import 'user.dart';

Future<User>? loadData(String url) async {
  http.Response resp = await http.get(Uri.parse(url));
  User userResult;
  print("Loading ..");
  if (resp.statusCode == 200) {
    String bodyData = resp.body;
    print(bodyData);
    Map<String, dynamic> dataMap = jsonDecode(bodyData);
    userResult = User.fromJSON(dataMap);
    return userResult;
  } else {
    return User.fromJSON(Map());
  }
}
