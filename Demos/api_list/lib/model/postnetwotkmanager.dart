import 'dart:convert';

import 'post.dart';
import 'package:http/http.dart' as http;

class PostNetworkManager {
  String url;
  PostNetworkManager({required this.url});
  Future<List<Post>> loadPosts() async {
    List<Post> finalList = List.empty(growable: true);
    http.Response resp = await http.get(Uri.parse(url));
    if (resp.statusCode == 200) {
      String body = resp.body;
      List jsonList = jsonDecode(body) as List;
      for (var jsonObject in jsonList) {
        print(jsonObject);
        Post currentPost = Post.fromMap(jsonObject);

        finalList.add(currentPost);
      }
      print("Data list has ${finalList.length}");
      return finalList;
    }
    throw Error();
  }
}
