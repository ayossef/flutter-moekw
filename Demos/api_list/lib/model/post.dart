class Post {
  int id;
  int userId;
  String title;
  String body;

  Post(
      {required this.id,
      required this.userId,
      required this.title,
      required this.body});

  factory Post.fromMap(Map<String, dynamic> data) {
    Post newPost = Post(
        id: data["id"],
        userId: data["userId"],
        title: data["title"],
        body: data["body"]);
    return newPost;
  }
}
