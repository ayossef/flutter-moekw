import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  String value;
  HeaderText({super.key, required this.value});

  @override
  Widget build(BuildContext context) {
    return Text(
      value,
      style: TextStyle(fontSize: 30, color: Colors.blueAccent),
    );
  }
}
