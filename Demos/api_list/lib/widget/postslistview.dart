import 'package:flutter/material.dart';

import '../model/post.dart';
import 'card.dart';

class PostsListView extends StatefulWidget {
  List<Post> posts = [];
  PostsListView({super.key, required this.posts});

  @override
  State<PostsListView> createState() => _PostsListViewState();
}

class _PostsListViewState extends State<PostsListView> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.posts.length,
        itemBuilder: ((context, index) {
          return CardItem(
              id: widget.posts[index].id,
              mainTitle: widget.posts[index].title,
              details: widget.posts[index].body);
        }));
  }
}
