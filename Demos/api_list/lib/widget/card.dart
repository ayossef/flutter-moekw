import 'package:flutter/material.dart';

class CardItem extends StatelessWidget {
  String mainTitle;
  String details;
  int id;
  CardItem(
      {super.key,
      required this.mainTitle,
      required this.details,
      required this.id});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      elevation: 4,
      child: Column(
        children: [
          ListTile(
            leading: Icon(Icons.star),
            title: Text(
              mainTitle,
              style: TextStyle(fontSize: 20),
            ),
            trailing: Text(
              '$id',
              style: TextStyle(fontSize: 20),
            ),
            subtitle: Text(
              details,
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.all(15.0),
          //   child: TextButton(
          //     onPressed: () {},
          //     style: TextButton.styleFrom(
          //         backgroundColor: Colors.blue,
          //         minimumSize: Size(MediaQuery.of(context).size.width, 56)),
          //     child: const Text(
          //       'Add to cart',
          //       style: TextStyle(color: Colors.white),
          //     ),
          //   ),
          // )
        ],
      ),
    );
  }
}
