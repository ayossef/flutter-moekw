import 'package:api_list/model/postnetwotkmanager.dart';
import 'package:flutter/material.dart';

import '../model/post.dart';
import '../widget/postslistview.dart';

class PostsList extends StatefulWidget {
  const PostsList({super.key});

  @override
  State<PostsList> createState() => _PostsListState();
}

class _PostsListState extends State<PostsList> {
  Future<List<Post>>? _futurePostsList = null;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _futurePostsList =
        PostNetworkManager(url: "https://jsonplaceholder.typicode.com/posts")
            .loadPosts();
  }

  void _loadData() {
    setState(() {
      _futurePostsList =
          PostNetworkManager(url: "https://jsonplaceholder.typicode.com/posts")
              .loadPosts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PostsList'),
      ),
      body: Center(
        child: FutureBuilder(
          future: _futurePostsList,
          initialData: null,
          builder: (context, snapshot) {
            if (snapshot.connectionState.index == 1) {
              return CircularProgressIndicator();
            }
            if (snapshot.hasData) {
              print("Has Data");
              return PostsListView(posts: snapshot.data!);
            } else {
              return Text("...");
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _loadData,
        child: Icon(Icons.refresh),
      ),
    );
  }
}
