# Posts List App

we need to view the list of posts. list of posts are loaded from an external url

## Project Parts

### model
- Post class - model class
  - data (id, title, body, userid)
  - methods:
    - constructor
    - fromMap -> object
- NetworkLoader
  - data (url)
  - Future<List<Post>> loadData(url)
    - resp = http.get
    - check resp.statusCode == 200
    - jsonBody = resp.body
    - List<Map> = jsonDecode(jsonBody)
    - loop over List<Map> for each map Post.fromMap
    - finalList.add(newPost)
    - return finalList

### pages
- HomeList
  - Scaffload
    - body
      - FutureBuilder: 
        - Future<List<Post>>
        - builder
          - connectionStatus
          - hasData
            - return ListView.builder:
              - itemCount: snapshot.data!.length
              - itemBuilder: return Card(snapshot.data![index].id, snapshot.data!.title,snapshot.data![index].body)

### widgets