# Flutter Installation Toolkit

## 1. Install Flutter SDK
download and install flutter using the link

https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_3.19.6-stable.zip

## 2. Download and Install Supporting Tools
 ### Download and install git 
 using the following link

 https://gitforwindows.org/

 ### Download and install VS code
 using the following link
https://code.visualstudio.com/docs/setup/windows

## 3. Verify the installation using 
```cmd
flutter doctor
```

## 4. Finally, create a new project
using the command 
```cmd
flutter create myapp
cd myapp
flutter run
```